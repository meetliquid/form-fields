# Form Fields Plugin #

Enable custom styles, type, events on form fields
Custom html5 attributes allowed:

* data-holder:  set placeholder on inputs
* data-type:    set data type accepted (int, dni, email)
* data-valid:   set validate type
* data-limit:   set limit of characters


### Resume ###

 * Version: 0.5
 * Date: 21/06/2016
 * Author: Jim Peñaloza

### Usage ###

```
#!javascript
$.formFields();
```