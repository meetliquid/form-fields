/* 
 * Form Fields Plugin
 * 
 * Enable custom styles, type, events on form fields
 * Custom html5 attributes allowed:
 *
 *      * data-holder:  set placeholder on inputs
 *      * data-type:    set data type accepted (int, dni, email)
 *      * data-valid:   set validate type
 *      * data-limit:   set limit of characters
 * 
 * Version: 0.5
 * Date: 21/06/2016
 * Author: Jim Peñaloza
 * 
 */

;(function( $ ) {
    
    // declare plugin
    $.formFields = function(element, options) {
        
        // default options
        var defaults = {
            
            // default clasess
            focusClass: 'focus',
            validClass: 'valid',
            errorClass: 'error',
            checkClass: 'checked'
        }
        
        // current plugin
        var plugin = this;
    
    
        // settings
        plugin.settings = {};
        
        var $element = $(element);
        var element = element;
    
    
        // init plugin
        plugin.init = function() {
            // merge settings
            plugin.settings = $.extend({}, defaults, options);
            
            // render input / textarea
            $(".input input, .area textarea").each(function(i, e) {
                var $this = $(this);
                var $input = $this.parent();
                var $holder = $this.attr("data-holder");
                
                // default value
                if ($this.val() == "") { 
                    $this.val($holder); 
                }
                // focus events
                $this.focus(function() { 
                    $input.addClass( plugin.settings.focusClass ); 
                    if ($this.val() == $holder) { $this.val(""); } 
                });
                $this.blur(function() { 
                    $input.removeClass( plugin.settings.focusClass ); 
                    if ($this.val() == "") { $this.val($holder); }
                });
            });
            
            // render select
            $(".pull select").each(function(i, e) {
                var $this = $(this);
                var $pull = $this.parent();
                
                // default value
                if ($pull.find("div").length == 0) {
                    $pull.prepend("<div>"+$this.children(":selected").text()+"</div>");
                }
                // change event
                $this.change(function(){
                    $pull.children("div").text($this.children(":selected").text());
                });
                // focus event
                $this.focus(function() { 
                    $pull.addClass( plugin.settings.focusClass ); 
                });
                $this.blur(function() { 
                    $pull.removeClass( plugin.settings.focusClass ); 
                });
            });
            
            // render checkbox 
            $('.check input').each(function(i, e){
                var $this = $(this);
                var $input = $this.parent();
                
                $this.on("change", function(){
                    if ($this.is(":checked")) {
                        $input.addClass( plugin.settings.checkClass );
                    } else {
                        $input.removeClass( plugin.settings.checkClass );
                    }
                });
                
                if ($this.is(":checked")) {
                    $input.addClass( plugin.settings.checkClass );
                } else {
                    $input.removeClass( plugin.settings.checkClass );
                }
            });
            
            // render radio
            $('.radio input').each(function(i,e){
                var $this = $(this);
                var $input = $this.parent();
                var $name = $this.attr("name");
                
                $this.on("change", function(){
                    $(".radio input:radio[name="+$name+"]").each(function(i,e){
                        var $check = $(this);
                        if ($check.is(":checked")) {
                            $check.parent().addClass("checked");
                        } else {
                            $check.parent().removeClass("checked");
                        }
                    });
                });
                
                if ($this.is(":checked")) {
                    $input.addClass( plugin.settings.checkClass );
                } else {
                    $input.removeClass( plugin.settings.checkClass );
                }
            });
        }
        
        // public methods
        plugin.reset = function() {
            // reset all fields
        }
        
        // private methods
        var privateMethod  = function() {
            // test method
        }
        
        // fire the plugin
        plugin.init();
    
    }
 
    // add plugin to jQuery.fn
    $.fn.formFields = function(options) {
        
        return this.each(function(options){
           
        });
        
    }
})( jQuery );